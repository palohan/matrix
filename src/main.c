#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <ctype.h>

#include "matrix.h"
#include "utils.h"


enum textKey
{
	ENTER_ROWS, 
	ENTER_COLS, 
	ENTER_ROWS_LEFT,
	ENTER_COLS_LEFT,
	ENTER_ROWS_RIGHT,
	ENTER_COLS_RIGHT,
	ENTER_DIMS
};

//MAKRA
const char* textSK[] =
{
	"Zadaj pocet riadkov matice {range}: ",
	"Zadaj pocet stlpcov matice {range}: ",
	"Zadaj pocet riadkov lavej matice {range}: ",
	"Zadaj pocet stlpcov lavej matice {range}: ",
	"Zadaj pocet riadkov pravej matice {range}: ",
	"Zadaj pocet stlpcov pravej matice {range}: ",
	"Zadaj rozmer matice {range}: "
};

const char** arrText = textSK;

//DEKLARACIE
//funcke - DEKLARUJEME tu, aby ich kompilator poznal

//DEKLARACIE TYPOV
enum optionTrait
{
	TWO_MAT = 1,
	ONE_MAT = 2,
	OTHER_MAT_ASK_ROW = 4,
	OTHER_MAT_ASK_COL = 8,
	SHOW_VALUE = 16
};							//operacia pracuje bud z 1 alebo 2 maticami

enum mainOptions { ADD, SUBTRACT, MULTIPLY, DETERMINANT, SETUP, EXIT };	//enumeracia operacii pre menu
enum setOptions { PRINT_DEBUG, GENERATE_VALS, MAT_DIMS, VAL_RANGE, RESET, BACK };
enum rangeOptions { MIN_VAL, MAX_VAL };


typedef void (*procMenu)(const struct option*);						//pointer na funkciu

//struct option																//deklarovana v matrix.h
//{
//    const int operation;
//    const int trait;
//    const char* const m_name;
//    const struct menu* const linkedMenu;
//	int value;
//};

struct menu
{
    const int m_firstOptionPrintValue;
	const int m_optionsLength;
    const char* const m_name;
    struct option* const m_options;
    const procMenu m_handler;
};


//GLOBALNE PREMENE
struct option rangeMenuArr[] =
{
    {MIN_VAL,   SHOW_VALUE, "min hodnota"},
    {MAX_VAL,	SHOW_VALUE, "max hodnota"},    
	//{RESET,			0,		"reset nastavenia"},
    {BACK,          0,		"NAZAD"}
};

void rangeMenuHandler(const struct option* pVolba);
const struct menu rangeMenu = { 1, ARRAY_SIZE(rangeMenuArr), "Rozsahy", rangeMenuArr, &rangeMenuHandler };


struct option setMenuArr[] =
{
    {PRINT_DEBUG,	SHOW_VALUE, "ladiace hlasky"},
	{GENERATE_VALS,	SHOW_VALUE, "generovanie hodnot"},
    {MAT_DIMS,      SHOW_VALUE, "rozmery matice"},
    {VAL_RANGE,     SHOW_VALUE,	"rozsah hodnot matice", &rangeMenu},
	{RESET,			0,			"reset nastavenia"},
    {BACK,          0,			"NAZAD"}
};

void setMenuHandler(const struct option* pVolba);
const struct menu setMenu = {1, ARRAY_SIZE(setMenuArr), "NASTAVENIA", setMenuArr, &setMenuHandler};


struct option mainMenuArr[] =
{
    {ADD,           TWO_MAT,    "sucet"			},
    {SUBTRACT,      TWO_MAT,    "rozdiel"			},
    {MULTIPLY,      TWO_MAT | OTHER_MAT_ASK_COL,    "sucin"				},
    {DETERMINANT,   ONE_MAT,    "determinant"       },
    {SETUP,         0,          "nastavenia",	&setMenu},
    {EXIT,          0,          "KONIEC"            },
};

void mainMenuHandler(const struct option* pVolba);
const struct menu mainMenu = {1, ARRAY_SIZE(mainMenuArr), "HLAVNE MENU", mainMenuArr, &mainMenuHandler};


AppSettings appSettings = 
{
	.m_dbgVar =			&setMenuArr[PRINT_DEBUG],
	.m_generateON =		&setMenuArr[GENERATE_VALS],
	.m_maxRozmerMatice =	&setMenuArr[MAT_DIMS],
	.m_maxHodnotaMatice = &rangeMenuArr[MAX_VAL],
	.m_minHodnotaMatice = &rangeMenuArr[MIN_VAL]
};

extern int tabLevelGlob;

void resetAppSettings()
{
	appSettings.m_dbgVar->value = 0;
	appSettings.m_generateON->value = 1;
	appSettings.m_maxRozmerMatice->value = MAX_DIMENSION_DEF;
	appSettings.m_maxHodnotaMatice->value = MAX_MAT_VALUE_DEF;
	appSettings.m_minHodnotaMatice->value = -MAX_MAT_VALUE_DEF;
}

void printOptionVals(const struct menu* const m)
{
	const struct option* pVolba = 0;

	for (int i = 0; i < m->m_optionsLength; ++i)
	{
		pVolba = &m->m_options[i];

		if (pVolba->trait == SHOW_VALUE)
		{
			if (pVolba->linkedMenu)
				printOptionVals(pVolba->linkedMenu);
			else
				printf(" %s = %d;", pVolba->m_name, pVolba->value);
		}
	}
}


int processMenu(const struct menu* const m, const int tabLevel)
{
	tabLevelGlob = tabLevel;

	const struct option* pVolba = 0;
	const int lastOption = m->m_firstOptionPrintValue + m->m_optionsLength - 1;
	int volba;
	
	while (1)
	{
		puts("");
		printTabs(tabLevel);
		puts(m->m_name);

		for (int i = 0; i < m->m_optionsLength; ++i)
		{
			pVolba = &m->m_options[i];

			if (pVolba->trait == SHOW_VALUE)
			{
				tabbedPrintf("\t%u - %s (", i + m->m_firstOptionPrintValue, pVolba->m_name);

				if (pVolba->linkedMenu)
					printOptionVals(pVolba->linkedMenu);
				else
					printf(" = %d", pVolba->value);

				puts(")");
			}
			else
				tabbedPrintf("\t%u - %s\n", i + m->m_firstOptionPrintValue, pVolba->m_name);
		}
		
		//VSTUP - VOLBA
		puts("");
		volba = readNumber("Zadaj volbu: ", tabLevel, m->m_firstOptionPrintValue, lastOption);
		puts("");		

		if (volba == lastOption)
			return 0;

		pVolba = &m->m_options[volba - m->m_firstOptionPrintValue];

		if (pVolba->linkedMenu)
			processMenu(pVolba->linkedMenu, tabLevel + 1);
		
		tabLevelGlob = tabLevel;
		m->m_handler(pVolba);
	}
}


void rangeMenuHandler(const struct option* pVolba)
{
	switch (pVolba->operation)
    {
        case MAX_VAL:
            appSettings.m_maxHodnotaMatice->value = readNumber("Nastav maximalny rozsah hodnot matic {range}: ", tabLevelGlob, appSettings.m_minHodnotaMatice->value + 1, MAX_MAT_VALUE);
            break;

        case MIN_VAL:
            appSettings.m_minHodnotaMatice->value = readNumber("Nastav minimalny rozsah hodnot matic {range}: ", tabLevelGlob, -MAX_MAT_VALUE, appSettings.m_maxHodnotaMatice->value - 1);
            break;
			        
        /*case RESET:
			if (readNumber("resetnut nastavenia?", tabLevelGlob, 1, 0, 1))
				resetRangeSettings();
            break;*/
    }
}


void setMenuHandler(const struct option* pVolba)
{
    switch (pVolba->operation)
    {
        case MAT_DIMS:
            appSettings.m_maxRozmerMatice->value = readNumber("Zadaj maximalny rozmer matic {range}: ", tabLevelGlob, MIN_DIMENSION + 1, MAX_DIMENSION);
            break;

        case PRINT_DEBUG:
            appSettings.m_dbgVar->value = readNumber("Zapni ladiace hlasky {range}: ", tabLevelGlob, 0, 1);
            break;			

        case GENERATE_VALS:
            appSettings.m_generateON->value = readNumber("Zapni generovanie hodnot {range}: ", tabLevelGlob, 0, 1);
            break;

        case RESET:
			if (readNumber("resetni nastavenia {range}: ", tabLevelGlob, 0, 1))
				resetAppSettings();
            break;
    }
}


void mainMenuHandler(const struct option* pVolba)
{
    int rowCount, colCount;
	bool status = false;
	const int trait = pVolba->trait;

    if (trait & (ONE_MAT | TWO_MAT))
    {
        if (pVolba->operation == DETERMINANT)
        {
            rowCount = colCount = readNumber(arrText[ENTER_DIMS], tabLevelGlob, MIN_DIMENSION, MAX_DET_DIMENSION);
        }
		else
		{
			bool askBothMats = trait & (OTHER_MAT_ASK_ROW | OTHER_MAT_ASK_COL);

			rowCount = readNumber(arrText[askBothMats ? ENTER_ROWS_LEFT : ENTER_ROWS], tabLevelGlob, MIN_DIMENSION, appSettings.m_maxRozmerMatice->value);
			colCount = readNumber(arrText[askBothMats ? ENTER_COLS_LEFT : ENTER_COLS], tabLevelGlob, MIN_DIMENSION, appSettings.m_maxRozmerMatice->value);
		}


        srand(time(0));         //aby boli skutocne nahodne cisla

		int scalarValue = 0;
		matrix left = { 0 }, right = { 0 }, result = { 0 };					//deklarujeme 3 struktury matice
        createMatrix(&left, rowCount, colCount);					//vyhradime pamat
        fillMatrix(&left, appSettings.m_minHodnotaMatice->value, appSettings.m_maxHodnotaMatice->value);	//naplnime cislami

		puts("");
		tabbedPuts(pVolba->trait & ONE_MAT ? "MATICA" : "LAVA MATICA");
		printMatrix(&left);


		if (trait & TWO_MAT)														//ak mame 2 matice, rovnako vyrobime aj druhu
		{
			if (trait & OTHER_MAT_ASK_ROW)
				rowCount = readNumber(arrText[ENTER_ROWS_RIGHT], tabLevelGlob, MIN_DIMENSION, appSettings.m_maxRozmerMatice->value);

			if (trait & OTHER_MAT_ASK_COL)
				colCount = readNumber(arrText[ENTER_COLS_RIGHT], tabLevelGlob, MIN_DIMENSION, appSettings.m_maxRozmerMatice->value);

			if (pVolba->operation == MULTIPLY)
				rowCount = left.m_colCount;

			createMatrix(&right, rowCount, colCount);
			fillMatrix(&right, appSettings.m_minHodnotaMatice->value, appSettings.m_maxHodnotaMatice->value);

			tabbedPuts("PRAVA MATICA");
			printMatrix(&right);
		}
		

		switch (pVolba->operation)
		{
			case ADD:
				status = addMatrices(&result, &left, &right);
				break;

			case SUBTRACT:
				status = subtractMatrices(&result, &left, &right);
				break;

			case MULTIPLY:
				status = multiplyMatrices(&result, &left, &right);
				break;

			case DETERMINANT:
				status = getDeterminant(&left, &scalarValue);
				break;
		}

		if (status == true)
        {
            printf("VYSLEDOK - %s:\n", pVolba->m_name);

			if (pVolba->trait & TWO_MAT)
			{
				printMatrix(&result);
				releaseMatrix(&result);
			}
			else
				printf("%d\n", scalarValue);
        }


		releaseMatrix(&left);					//uvolnime pamet po maticiach
		releaseMatrix(&right);
    }
}


int main(int argc, char* argv[])
{
	resetAppSettings();

    processMenu(&mainMenu, 0);

	return 0;
}