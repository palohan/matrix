#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "matrix.h"
#include "utils.h"


extern AppSettings appSettings;					//odkaz na globalnu premennu definovanu v inom subore. tymto bude spolocna v celom programe.
extern int tabLevelGlob;


void createMatrix(matrix* mat, unsigned int rowCount, unsigned int colCount)
{
	assert(checkValidRange(rowCount, colCount));

	mat->m_rowCount = rowCount;
	mat->m_colCount = colCount;

	mat->m_rows = (int**)malloc(rowCount * sizeof(int*));

	foreachRow(mat)
		mat->m_rows[row] = (int*)malloc(colCount * sizeof(int));

	initMatrix(mat, 0);
}


void releaseMatrix(matrix* mat)
{
	if (mat->m_rows)
	{
		foreachRow(mat)
			free(mat->m_rows[row]);

		free(mat->m_rows);
		
		mat->m_rows = 0;
		mat->m_rowCount = 0;
		mat->m_colCount = 0;
	}
}


void reCreateMatrix(matrix* mat, unsigned int rowCount, unsigned int colCount)
{
	//if (!checkMatDims(mat, rowCount, colCount))
	{
		releaseMatrix(mat);
		createMatrix(mat, rowCount, colCount);
	}
}


bool initMatrix(matrix* mat, int val)
{
	myAssertInt(checkMatRange(mat), WRONG_DIMS, false);

	foreachVal(mat) = val;

	return true;
}


bool fillMatrix(matrix* mat, int minVal, int maxVal)
{
	if (appSettings.m_generateON->value)
		return generateMatrix(mat, minVal, maxVal);
	else
		return readMatrix(mat, minVal, maxVal);
}


bool generateMatrix(matrix* mat, int minVal, int maxVal)
{
	myAssertInt(checkMatRange(mat), WRONG_DIMS, false);

	foreachVal(mat) = rand() % (maxVal - minVal + 1) + minVal;

	return true;
}


bool readMatrix(matrix* mat, int minVal, int maxVal)
{
	myAssertInt(checkMatRange(mat), WRONG_DIMS, false);

	static char buf[50];

	foreach(mat)
	{
		sprintf(buf, "matrix[%u, %u] {range} = ", row, col);
		matVal(mat)	= readNumber(buf, tabLevelGlob, 4, minVal, maxVal);
	}	

	return true;
}


void printMatrix(const matrix* mat)
{
	myAssert(!isEmptyMatrix(mat) && checkMatRange(mat), WRONG_DIMS);

	foreachRow(mat)
	{
		foreachCol(mat)
			tabbedPrintf("%d\t", matVal(mat));

		puts("");
	}
	puts("");
}


matrix* cloneMatrix(const matrix* other)
{
	matrix* mat = (matrix*)malloc(sizeof(matrix));
	createMatrix(mat, getDimensions(other));

	copyMatrix(mat, other);

	return mat;
}


void copyMatrix(const matrix* left, const matrix* right)
{
	myAssert(checkMatsDims(left, right), WRONG_DIMS);

	foreachVal(left) = matVal(right);
}


void negateMatrix(matrix* mat)
{
	foreachVal(mat) *= -1;
}


bool addMatrices(matrix* result, const matrix* left, const matrix* right)
{
	myAssertInt(checkMatsDims(left, right), WRONG_DIMS, false);

	reCreateMatrix(result, left->m_rowCount, left->m_colCount);

	foreachVal(result) = matVal(left) + matVal(right);

	return true;
}


bool subtractMatrices(matrix* result, const matrix* left, const matrix* right)
{
	myAssertInt(checkMatsDims(left, right), WRONG_DIMS, false);

	reCreateMatrix(result, left->m_rowCount, left->m_colCount);

	foreachVal(result) = matVal(left) - matVal(right);

	return true;
}


bool multiplyMatrices(matrix* result, const matrix* left, const matrix* right)
{
	//<x,y> * <y,z> = <x,z>
	myAssertInt(left->m_colCount == right->m_rowCount, WRONG_DIMS, false);

	int sameDim = left->m_colCount;

	reCreateMatrix(result, left->m_rowCount, right->m_colCount);

	foreach(result)
		for (int inPos = 0; inPos < sameDim; ++inPos)
			matVal(result) += left->m_rows[row][inPos] * right->m_rows[inPos][col];

	return true;
}


bool isEmptyMatrix(const matrix* mat)
{
	return mat->m_colCount == 0 && mat->m_rowCount == 0 && mat->m_rows == 0;
}


bool isSquare(const matrix* mat)
{
	return !isEmptyMatrix(mat)  && mat->m_rowCount == mat->m_colCount;
}


int partialDeterminant(const matrix *mat, int smer)
{
	smer = smer ? 1 : -1;

	int result = 0,
		diagonal,
		cellVal,
		startCol = smer == 1 ? 0 : mat->m_colCount - 1;

	for (int col = 0; col < mat->m_rowCount; ++col, startCol += smer)
	{
		diagonal = 1;

		for (int row = 0, cellY = startCol; row < mat->m_colCount; ++row, cellY += smer)
		{
			if (smer == 1 && cellY >= mat->m_colCount || smer == -1 && cellY < 0)
				cellY += -1 * smer * mat->m_colCount;

			cellVal = mat->m_rows[row][cellY];

			dbgPrint("mat[%d, %d] = %d\n", row, cellY, cellVal);
			diagonal *= cellVal;
		}

		result += diagonal;

		dbgPrint("diagonal=%d, total=%d\n\n", diagonal, result);
	}

	dbgPrint("result=%d\n\n", result);
	return result;
}


bool getDeterminant(const matrix *mat, int* result)
{
	myAssertInt(isSquare(mat), NOT_SQUARE, false);

	*result = partialDeterminant(mat, 1);
	*result -= partialDeterminant(mat, 0);

	return true;
}