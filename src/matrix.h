#pragma once

//TU NADEFINUJ MIN A MAX ROZMERY MATIC
#define MIN_DIMENSION 2
#define MAX_DIMENSION 10
#define MAX_DIMENSION_DEF 5
#define MAX_DET_DIMENSION 3
#define MAX_MAT_VALUE 99
#define MAX_MAT_VALUE_DEF 10


#define foreachRow(mat) 	for (unsigned int row = 0; row < (mat)->m_rowCount; ++row)
#define foreachCol(mat)		for (unsigned int col = 0; col < (mat)->m_colCount; ++col)
#define foreach(mat) 		foreachRow(mat) foreachCol(mat)

#define matVal(mat) 		(mat)->m_rows[row][col]
#define foreachVal(mat) 	foreach(mat) matVal(mat)

#define getDimensions(mat) 	(mat)->m_rowCount, (mat)->m_colCount

#define checkDimRange(row) 			((row) >= MIN_DIMENSION && (row) <= appSettings.m_maxHodnotaMatice->value)
#define checkValidRange(row, col) 	(checkDimRange(row) && checkDimRange(col))
#define checkMatRange(mat) 			checkValidRange((mat)->m_rowCount, (mat)->m_colCount)

#define checkMatDims(mat, m_rows, cols) 	((mat)->m_rowCount == (m_rows) && (mat)->m_colCount == (cols))
#define checkMatsDims(mat1, mat2) 		(checkMatDims((mat1), (mat2)->m_rowCount, (mat2)->m_colCount))

#define WRONG_DIMS "matica ma nevyhovujuce rozmery"
#define NOT_SQUARE "matica nie je stvorcova"


#include <stdbool.h>

typedef struct
{
	unsigned int m_rowCount, m_colCount;
	int** m_rows;
} matrix;


void createMatrix(matrix* mat, unsigned int rowCount, unsigned int colCount);
void reCreateMatrix(matrix* mat, unsigned int rowCount, unsigned int colCount);


void releaseMatrix(matrix* mat);

bool initMatrix(matrix* mat, int val);
bool generateMatrix(matrix* mat, int minVal, int maxVal);
bool readMatrix(matrix* mat, int minVal, int maxVal);
bool fillMatrix(matrix* mat, int minVal, int maxVal);


void printMatrix(const matrix* mat);

void copyMatrix(const matrix* left, const matrix* right);

matrix* cloneMatrix(const matrix* other);


void negateMatrix(matrix* mat);


bool addMatrices(matrix* result, const matrix* left, const matrix* right);
bool subtractMatrices(matrix* result, const matrix* left, const matrix* right);
bool multiplyMatrices(matrix* result, const matrix* left, const matrix* right);


bool isEmptyMatrix(const matrix* mat);
bool isSquare(const matrix* mat);
bool getDeterminant(const matrix *mat, int* result);
