#pragma once

#include <stdbool.h>
#include <stdio.h>


#ifndef __FUNCTION_NAME__
	#ifdef WIN32   //WINDOWS
		#define __FUNCTION_NAME__   __FUNCTION__
	#else          //UNIX
		#define __FUNCTION_NAME__   __func__
	#endif
#endif

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#define _myAssert(expr, msg) 			if (!(expr)) {fprintf(stderr, "%s: %s\n", __FUNCTION_NAME__, msg); return
#define myAssert(expr, msg) 			_myAssert(expr, msg) ;}
#define myAssertInt(expr, msg, code) 	_myAssert(expr, msg) code;}

#define dbgPrint(...) 					if (appSettings.m_dbgVar->value) printf(__VA_ARGS__)

#define tabbedPrintfEx(stream, ...)		{ printTabs(tabLevelGlob); fprintf(stream, __VA_ARGS__); }
#define tabbedPrintf(...)				tabbedPrintfEx(stdout, __VA_ARGS__)

#define tabbedPutsEx(stream, buf)		{ printTabs(tabLevelGlob); fputs(buf, stream); fputs("\n", stream); }
#define tabbedPuts(buf)					{ printTabs(tabLevelGlob); puts(buf); }

#define RANGE_SPEC "{range}"

struct menu;
struct option
{
	const int operation;
	const int trait;
	const char* const m_name;
	const struct menu* const linkedMenu;
	int value;
};

typedef struct
{
	struct option* const m_dbgVar;
	struct option* const m_generateON;
	struct option* const m_maxRozmerMatice;
	struct option* const m_maxHodnotaMatice;
	struct option* const m_minHodnotaMatice;
} AppSettings;


/*
message,	string	- zakladna hlaska, kt sa este sformatuje a vytlaci pri kazdom vyzvani
tabLevel,	int		- uroven odsadenia TABmi od zaciatku riadku
minVal,		int		- minimalna pripustna hodnota
maxVal,		int		- maximalna pripustna hodnota
*/
int readNumber(const char* message, int tabLevel, int minVal, int maxVal);

void printTabsGlob();
void printTabs(const unsigned int tabLevel);