#include <string.h>

#include "utils.h"


//GLOBALS
int tabLevelGlob = 0;

void printTabsGlob()
{
	printTabs(tabLevelGlob);
}

void printTabs(const unsigned int tabLevel)
{
	for (unsigned int i = 0; i < tabLevel; i++)
		putchar('\t');
}


/*
message,	string	- zakladna hlaska, kt sa este sformatuje a vytlaci pri kazdom vyzvani
tabLevel,	int		- uroven odsadenia TABmi od zaciatku riadku
minVal,		int		- minimalna pripustna hodnota
maxVal,		int		- maximalna pripustna hodnota
*/
int readNumber(const char* message, int tabLevel, int minVal, int maxVal)
{
#define MAX_INPUT_LENGTH 10

	static char inputBuf[MAX_INPUT_LENGTH];
	static const int RANGE_SPEC_LEN = ARRAY_SIZE(RANGE_SPEC) - 1;
	char* endPtr = NULL;
	int result, c;
	
	while (1)
	{
		if (message)
		{
			printTabs(tabLevel);

			const char* from = message, * beg = 0;

			do
			{
				beg = strstr(from, RANGE_SPEC);
				if (beg)
				{
					printf("%.*s", (int)(beg - from), from);
					printf("<%d, %d>", minVal, maxVal);
					from = beg + RANGE_SPEC_LEN;
				}
				else
					printf("%s", from);
			}
			while (beg);
		}


		fgets(inputBuf, sizeof(inputBuf), stdin);

		if (strchr(inputBuf, '\n') == 0)											//presiahli sme MAX_INPUT_LENGTH a nenasli sme \n - presiahli sme nas buffer
		{
			while ((c = getchar()) != '\n' && c != EOF)								//musime skonzumovat vsetky zadane znaky
				;
			continue;
		}

		inputBuf[strlen(inputBuf) - 1] = '\0';										//remove \n

		result = strtol(inputBuf, &endPtr, 10);

		if (*endPtr)																//obsah vstupu nie je cislo
			continue;

		if (result < minVal || result > maxVal)										//cislo neni v rozsahu
			continue;

		return result;																//ak sme sa dostali sem, input je dobry
	}
}
