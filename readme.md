﻿# README #

### Úvod ###
Povodne semestrálna práca z C pre vysokoskolskeho kamaráta. Odzrkadľuje moje momentálne zručnosti v C (jar 2017). 

S kódom mozte robit cokolvek okrem zarábania penazi.

### Zhrnutie ###
Konzolová aplikacia, ktorá pracuje s maticami, napisana v C. Ovlada sa cez hierarchické menu; podporuje sucet, rozdiel, sucin a determinant matíc. Pri zadavani rozmerov a hodnôt matíc vyžaduje dodrzat urcene rozsahy hodnôt; tieto a iné nastavenia je mozne zmeniť v menu nastavení.

### Ako buildovat? ###
Vyzaduje sa minimalne C99.

Zdrojové súbory sú v priečinku *src*. Mame tieto možnosti:

- pridáme si súbory do nasho IDE
- otvorime si repozitar vo Visual Studio 2017, ktore podporuje cmake.
- cez terminál spustime cmake out-of-source build, napríklad: 
```
#!

cd matrix
mkdir build
cd build
cmake ..
make
```

# Have fun! #